require_relative "text_walker.rb"
require_relative "file_sorter.rb"
require 'csv'
require 'fileutils'

class ParserProcessor

  attr_reader :wd, :columns, :config, :reader, :page_key, :search, :page

  def initialize(config, columns, wd)
    @config = config
    @columns = columns
    @wd = wd
  end

  def process
    saved_columns = Marshal.dump(columns)
    Dir.chdir('..') if Dir.pwd == wd
    Dir[config[:file_path] + "*.pdf"].each do |file|
      puts "parsing #{file}"
      begin
        @reader = PDF::Reader.new(file)
        setup_folders
        find_search_values
        remove_unfound_values
        modify_values
        manage_csv
        manage_pdf(file)
        @columns = Marshal.load(saved_columns)
      rescue  Exception => e
        move_bad_pdf(file)
        #new_line_csv
        puts e.message
        puts e.backtrace
      end
    end
  end

  def find_search_values
    config[:search_keys].each do |search|
      @search = search
      @page_key = search.fetch(:page, :all)
      if page_key == :all
        search_each_page
      else
        @page = reader.pages[page_key-1]
        search_single_page
      end
    end

  end

  def search_each_page
    reader.pages[1..-1].each do |page|
      @page = page
      search_single_page
    end
  end

  def search_single_page
    if page_key == 1
      find_text_through_walker
    else
      find_text_in_page
    end
  end

  def find_text_in_page
    text = get_text
    unless text.empty?
      value = find_value_with_options(text)
      set_value(value)
    end
  end

  def get_text
    text = page.text.split("\n").reject{|t| t == ""}
    text = text.reject {|t| t !~ /#{search.fetch(:name)}/}
    text = [text[search[:nth_entry] - 1]] if search.key?(:nth_entry)
    text
  end

  def get_index(text)
    text.rindex{|elem| elem =~ /#{search.fetch(:name)}/}
  end

  def find_value_with_options(text)
    case
    when search[:special]
      special(search[:special])
    when search[:whole_line]
      value = text.first.strip
    when search[:money]
      value = search_money
    when search[:first]
      value = text.first.split("#{search.fetch(:name)}").reject{|ele| ele == ""}.first.split(" ").first.strip
    when search[:until]
      value = search_until(text)
    when search[:number]
      value = search_number(text)
    else
      value = text.first.split("#{search.fetch(:name)}").reject{|ele| ele == ""}.last.strip
    end
    value
  end

  def search_number(text)
    text.first.scan(/\d+/).first
  end

  def search_until(text)
    value = text.first.split("#{search.fetch(:name)}").reject{|ele| ele == ""}.last.strip
    values = [value]
    text = page.text.split("\n").reject{|ele| ele == ""}
    index = get_index(text)
    10.times do |count|
      if text[index + count + 1].scan(/#{search.fetch(:until)}/).any?
        break
      else
        values << text[index + count + 1].strip
      end
    end
    values.join("\n")
  end

  def search_money
    text = page.text.split("\n").reject{|ele| ele == ""}
    index = get_index(text)
    val = text[index].scan(/([0-9]([0-9,])*)(\.\d{2})?/)
    next_line_val = text[index + 1].scan(/([0-9]([0-9,])*)(\.\d{2})?/)
    if val.any?
      "$#{val.first.first}"
    else
      "$#{next_line_val.first.first}"
    end
  end

  def find_text_through_walker
    receiver = TextWalker.new
    page.walk(receiver)
    values = receiver.values
    if search.fetch(:name) == "RENEWAL NEW POLICY"
      renewal = values.reject{|elem| elem !~ /RENEWAL/}
      if renewal.any?
        set_value("RENEWAL")
      else
        set_value("NEW POLICY")
      end
    elsif search[:date]
      value = search_date(values)
      set_value(value)
    else
    value = values.rindex{|elem| elem =~ /#{search.fetch(:name)}/}
    set_value(values[value + search.fetch(:line_after, 1)])
    end
  end

  def special(value)
    case
    when value == :NSW
      address = columns.select{|elem| elem[:name] == "Address"}.pop[:value]
      address.scan(/NSW|nsw/).any? ? set_value("NSW") : ''
    end
  end

  def search_date(values)
    index = values.rindex{|elem| elem =~ /#{search.fetch(:name)}/}
    value = []
    20.times do |count|
      value = values[index + count + 1].scan(/\d{2}\/\d{2}\/\d{4}/)
      if value.any?
        value = value.first
        break
      end
    end
    value
  end

  def set_value(value)
    columns.select{|elem| elem[:value] == search[:id]}.each {|elem| elem[:value] = value}
  end

  def modify_values
    if config.fetch(:modify_values, false)
      apply_modify_values
    end
  end

  def apply_modify_values
    modify_values = config.fetch(:modify_values)
    modify_values.each do |key, value|
      modify_value_case(key, value)
    end
  end

  def modify_value_case(key, value)
    case key
    when :apply_matrix
      if legals_taken?(value[:section])
        apply_premium_matrix(value[:turnover], value[:premium])
      end
    when :state
      set_state(value[:id])
    when :post_code
      set_post_code(value[:id])
    when :from_date
      set_from_date(value[:id])
    when :calculate_legals_base_premium
      set_legals_base_premium(value[:worker_id], value[:base_premium_id])
      reduce_liability_and_pi_base_premium(value[:liab_base_id], value[:base_premium_id])
    end
  end

  def legals_taken?(section_id)
    section = get_col_for_mod(section_id, :section)
    if section[:value] == "SECTION B"
      return true
    end
  end

  def reduce_liability_and_pi_base_premium(liab_base_id, base_premium_id)
    liab_col = get_col_for_mod(liab_base_id, :liab_base_id)
    bp_col = get_col_for_mod(base_premium_id, :base_premium_id)
    bp_val_int = bp_col[:value].scan(/\d+/).first.to_i
    liab_val_int = remove_currency(liab_col[:value]).to_f
    liab_col[:value] = "$#{liab_val_int - bp_val_int}"
  end

  def set_legals_base_premium(work_id, prem_id)
    worker_cost_table = {1 => 50, 2 => 75, 3 => 75, 4 => 100, 5 => 100, 6 => 125, 7 => 125, 8 => 125, 9 => 125, 10 => 125}
    worker_col = get_col_for_mod(work_id, :worker_id)
    prem_col = get_col_for_mod(prem_id, :base_premium_id)
    worker_num = worker_col[:value].to_i
    if worker_num > 10
      prem_col[:value] = "$125"
    else
      prem_col[:value] = "$#{worker_cost_table[worker_num]}"
    end
  end

  def set_state(id)
    column = get_col_for_mod(id, :state)
    state = column[:value].scan(/NSW|QLD|VIC|SA|NT|TAS|WA/).first
    column[:value] = state
  end

  def set_from_date(id)
    column = get_col_for_mod(id, :from_date)
    from_date = column[:value].match(/(\d)+.*/)[0]
    column[:value] = from_date
  end

  def set_post_code(id)
    column = get_col_for_mod(id, :post_code)
    post_code = column[:value].scan(/\d{4}/).first
    column[:value] = post_code
  end

  def get_col_for_mod(id, sym)
    columns.reject{|ele| !ele[sym]}.select{|ele| ele[sym] == id}[0]
  end

  def remove_currency(ele)
    ele.tr("$", "").tr(",", "").tr(" ", "")
  end

  def apply_premium_matrix(turnover_id, premium_id)
    turnover = columns.reject{|ele| !ele[:turnover]}.select{|ele| ele[:turnover] == turnover_id}[0]
    premium = columns.reject{|ele| !ele[:premium]}.select{|ele| ele[:premium] == premium_id}[0]
    turnover_int = remove_currency(turnover[:value]).to_i
    premium_int = remove_currency(premium[:value]).to_i
    case turnover_int
    when 0..200000
      premium_int -= 280
    when 200001..450000
      premium_int -= 330
    when 450001..750000
      premium_int -= 390
    when 750001..1250000
      premium_int -= 460
    when 1250001..2000000
      premium_int -= 540
    end
    premium_value = "$#{premium_int.to_s.reverse.gsub(/(\d{3})(?=\d)/, '\\1,').reverse}"
    premium[:value] = premium_value
  end

  def concat_to_csv
    CSV.open(config[:file_name], 'a') do |csv|
      csv << columns.collect{|ele| ele[:value]}
    end
  end

  def create_csv
    CSV.open(config[:file_name], 'w') do |csv|
      headers = columns.collect{|ele| ele[:name]}
      csv << headers
      csv << columns.collect{|ele| ele[:value]}
    end
  end

  def manage_csv
    puts "Saving to file #{config[:file_name]}"
    if File.exist?(config[:file_name])
      concat_to_csv
    else
      create_csv
    end
  end

  def manage_pdf(file)
    FileUtils.mv(file, "#{config[:file_path]}parsed")
  end

  def setup_folders
    file_name = "#{config[:file_path]}parsed"
    FileUtils.mkdir(file_name) unless Dir.exist?(file_name)
  end

  def move_bad_pdf(file)
    FileUtils.mv(file, "./bad_pdf")
  end

  def new_line_csv
    CSV.open(config[:file_name], "wb", {:row_sep => "\r\n"})
  end

  def remove_unfound_values
    columns.each do |ele|
      value = ele[:value]
      if value && value.is_a?(Integer)
        ele[:value] = ""
      end
    end
  end

end
