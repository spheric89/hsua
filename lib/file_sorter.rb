class FileSorter
  attr_reader :files, :reader
  IDENTI = [{match: "05TP", folder: "trade_plus"}, {match: "01L", folder: "wrb_ilab"},
            {match: "02M", folder: "xl_mining"},  {match: "03L", folder: "xl_register"}]

  def initialize
    @files = Dir['./pdfs/*.pdf']
    process
  end

  def process
    files.each do |file|
      @reader = PDF::Reader.new(file)
      value = find_value_through_walker(file)
      match = find_match(value)
      setup_folder(match)
      move_file(file, match)
    end
  end

  def find_value_through_walker(file)
    receiver = TextWalker.new
    reader.pages[0].walk(receiver)
    values = receiver.values
    value = nil
    IDENTI.each do |ele|
      ele = ele.fetch(:match)
      value = values.rindex{|elem| elem =~ /#{ele}/}
      break if value
    end
    if value 
      return values[value]
    else 
      puts "#{file} has no policy number"
      return "bad_pdf"
    end
  end

  def find_match(value)
    if value != "bad_pdf"
      return IDENTI.select{|ele| "#{value}" =~ /#{ele[:match]}/}.first[:folder]
    else 
      "bad_pdf"
    end
  end

  def setup_folder(value)
    FileUtils.mkdir("./" + value) unless Dir.exist?("./" + value)
  end

  def move_file(file, value)
    FileUtils.mv(file, "./#{value}")
  end
end
