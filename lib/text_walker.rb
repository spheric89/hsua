class TextWalker
  attr_reader :values

  def show_text(string)
    @values ||= []
    @values << string
  end
end
