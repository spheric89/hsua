require 'pdf-reader'
require 'pry'
require_relative '../lib/pdf_parser_processor.rb'


begin
  FileSorter.new
  Dir.chdir('./config')
  wd = Dir.pwd
  Dir['../config/*'].each do |file|
    require_relative "#{file}"
    ParserProcessor.new(@config_file.config, @config_file.columns, wd).process
  end
rescue Exception => e
  puts e.message
  puts e.backtrace
else
  # other exception
ensure
  # always executed
end
