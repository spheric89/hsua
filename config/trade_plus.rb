module TradePlus

  class Setup
    attr_reader :config, :columns
    def initialize
      @config = CONFIG
      @columns = COLUMNS
    end

    CONFIG = {
      file_path: "./trade_plus/",
      file_name: "./trade_plus/trade_plus.csv",
      search_keys:
      [
        {id: 1, name: "Invoice Date", page: 1, date: true},
        {id: 2, name: "Policy No", page: 1},
        {id: 3, name: "RENEWAL NEW POLICY", page: 1},
        {id: 4, name: "From:", page: 3},
        {id: 5, name: "To:", page: 3},
        {id: 6, name: "The Insured:", page: 3, until: "Address of the Insured:", nth_entry: 2},
        {id: 7, name: "Address of the Insured:", page: 3, until: "Premium:"},
        {id: 8, name: "Premium:", page: 3},
        {id: 9, name: "BUSINESS DESCRIPTION:", page: 4, until: "NUMBER OF OPERATIVES:"},
        {id: 11, name: "LIMITS OF LIABILITY:", page: 4, money: true},
        {id: 12, name: "Goods in Care Custody and Control :"},
        {id: 13, name: "Limit of Indemnity:", page: 4, first: true},
        {id: 14, name: "Worker Related Claims", page: 4, money: true},
        {id: 15, name: "All other Claims:", page: 4, money: true},
        {id: 16, name: "Turnover:", page: 5},
        {id: 17, name: "SECTION C", whole_line: true},
        {id: 18, name: "Total Limit insured", page: 8},
        {id: 19, name: "Base Premium", page: 8},
        {id: 20, name: "NSW", special: :NSW},
        {id: 21, name: "Manual Directors/Employees/Labour Only Sub-Contractors/Work Experience", page: 4, number: true},
        {id: 22, name: "Total Limit insured", page: 8},
        {id: 23, name: "Base Premium", page: 8}],
      modify_values: {state: {id: 1},
                         post_code: {id: 1},
                         calculate_legals_base_premium: {worker_id: 1, base_premium_id: 1, liab_base_id: 1}}
    }


    COLUMNS =
      [{name: "Pol Schedule Sent Invoiced", value: 1},
       {name: "Comments/Reason"},
       {name: "Renewal Endorsement", value: 3},
       {name: "Internal Policy Number", value: 2},
       {name: "Name", value: 6},
       {name: "Liability Limit", value: 11},
       {name: "PI Limit", value: 13},
       {name: "Goods in CCC", value: 12},
       {name: "Liability & Base Premium", value: 8, liab_base_id: 1},
       {name: "Legals Base Premium", base_premium_id: 1},
       {name: "Total Base Premium", value: 8},
       {name: "Deductions - Overseas Tax 3%"},
       {name: "Deductions - Commission 25%"},
       {name: "Premium payable to Miles Smith"},
       {name: "Worker to Worker Excess", value: 14},
       {name: "Other Excess", value: 15},
       {name: "Business Description", value: 9},
       {name: "Period of Insurance From", value: 4},
       {name: "Period of Insurance To", value: 5},
       {name: "Gross Turnover", value: 16},
       {name: "Internal Policy Number", value: 2},
       {name: "Paid by client"},
       {name: "INCLUDED ON Risk Attachment BORDX"},
       {name: "Payment Bordx"},
       {name: "Address", value: 7},
       {name: "NSW", value: 20},
       {name: "Manual Workers?", value: 21, worker_id: 1},
       {name: "Tools of Trade", value: 17},
       {name: "Total Value of Tools", value: 22},
       {name: "Tools - Base Premium", value: 23},
       {name: "Post Code", value: 7, post_code: 1},
       {name: "State", value: 7, state: 1}
    ]
  end
end

@config_file = TradePlus::Setup.new
