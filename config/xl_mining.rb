module XlMining

  class Setup
    attr_reader :config, :columns
    def initialize
      @config = CONFIG
      @columns = COLUMNS
    end


    CONFIG = {
      file_path: "./xl_mining/",
      file_name: "./xl_mining/xl_mining.csv",
      search_keys:
      [
        {id: 1, name: "Invoice Date", page: 1, date: true},
        {id: 2, name: "Policy No:", page: 3},
        {id: 3, name: "RENEWAL NEW POLICY", page: 1},
        {id: 4, name: "From:", page: 3},
        {id: 5, name: "To:", page: 3},
        {id: 6, name: "The Assured:", page: 3},
        {id: 7, name: "Address of the Assured:", page: 3, until: "Rate:"},
        {id: 8, name: "Premium:", page: 3},
        {id: 9, name: "OCCUPATION:", page: 4, until: "ESTIMATED TURNOVER"},
        {id: 11, name: "LIMITS OF LIABILITY:", page: 4, money: true},
        {id: 12, name: "GOODS IN CARE CUSTODY & CONTROL:"},
        {id: 13, name: "Limit of Indemnity:", page: 4, first: true},
        {id: 16, name: "ESTIMATED TURNOVER:", page: 4},
        {id: 17, name: "SECTION B", whole_line: true},
        {id: 18, name: "Worker Related Claims", page: 4, money: true},
        {id: 19, name: "All other Claims:", page: 4, money: true},
        {id: 20, name: "NSW", special: :NSW},
      ],
      modify_values: {apply_matrix: {section: 1, turnover: 1, premium: 1}}
    }

    COLUMNS =
      [{name: "Pol Schedule Sent Invoiced", value: 1},
       {name: "Comments/Reason"},
       {name: "Renewal Endorsement", value: 3},
       {name: "Internal Policy Number", value: 2},
       {name: "Name", value: 6},
       {name: "Liability Limit", value: 11},
       {name: "PI Limit", value: 13},
       {name: "Goods in CCC", value: 12},
       {name: "Plant and Machinery & Tools of Trade Limit"},
       {name: "Base Premium", value: 8, premium: 1},
       {name: "Deductions - Overseas Tax 3%"},
       {name: "Deductions - Commission 25%"},
       {name: "Premium payable to Miles Smith"},
       {name: "Worker to Worker Excess", value: 18},
       {name: "Other Excess", value: 19},
       {name: "Business Description", value: 9},
       {name: "No Claims Confirmation"},
       {name: "Period of Insurance From", value: 4},
       {name: "Period of Insurance To", value: 5},
       {name: "Gross Turnover", value: 16, turnover: 1},
       {name: "Internal Policy Number", value: 2},
       {name: "Paid by client"},
       {name: "INCLUDED ON Risk Attachment BORDX"},
       {name: "Payment Bordx"},
       {name: "Address", value: 7},
       {name: "NSW", value: 20},
       {name: "Outside Australia"},
       {name: "Legals?", value: 17, section: 1}
    ]

  end
end

@config_file = XlMining::Setup.new
