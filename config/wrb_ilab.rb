module WrbIlab

  class Setup
    attr_reader :config, :columns
    def initialize
      @config = CONFIG
      @columns = COLUMNS
    end

    CONFIG = {
      file_path: "./wrb_ilab/",
      file_name: "./wrb_ilab/wrb_ilab.csv",
      search_keys:
      [
        {id: 1, name: "Invoice Date", page: 1, date: true},
        {id: 2, name: "Policy No", page: 1},
        {id: 3, name: "RENEWAL NEW POLICY", page: 1},
        {id: 4, name: "FROM", page: 3},
        {id: 6, name: "INSURED:", page: 3},
        {id: 7, name: "BASE OF OPERATION:", page: 3, until: "ESTIMATED TURNOVER"},
        {id: 8, name: "OCCUPATION:", page: 3, until: "BASE OF OPERATION"},
        {id: 9, name: "LIMITS OF LIABILITY:", page: 3, money: true},
        {id: 10, name: "EXCESSES:", page: 4, money: true},
        {id: 11, name: "ESTIMATED TURNOVER:", page: 3},
        {id: 12, name: "SECTION B", whole_line: true},
        {id: 13, name: "Base Premium", page: 3, money: true},
        {id: 14, name: "NSW", special: :NSW},
      ],
      modify_values: {apply_matrix: {section: 1, turnover: 1, premium: 1},
                      state: {id: 1},
                      post_code: {id: 1}, 
                      strip: {id: 1},
                      from_date: {id: 1}
                     }
    }

    COLUMNS =
      [
       {name: "Name of Insured", value: 6},
       {name: "State", value: 7, state: 1},
       {name: "Zip Code", value: 7, post_code: 1},
       {name: "Policy Number", value: 2},
       {name: "HSUA Binder"},
       {name: "Inception Date", value: 4, from_date: 1},
       {name: "Policy Period (mths)"},
       {name: "Transaction Type (New / Rnl / Mid-term)", value: 3},
       {name: "Base Premium", value: 13, premium: 1},
       {name: "Deductions - 3% overseas tax 3%"},
       {name: "Deductions - Commission 20%"},
       {name: "Premium payable to Miles Smith"},
       {name: "Contract Certain (Y/N)"},
       {name: "Policy Issue Date", value: 1},
       {name: "Reason for Failure"},
       {name: "Public Liability Limit / SI", value: 9},
       {name: "Professional Indemnity Limit"},
       {name: "Excess / Deductible", value: 10},
       {name: "Profession / Business", value: 8},
       {name: "Address", value: 7},
       {name: "Included on Cover Bordx"},
       {name: "Month Paid and noted on payment bordx"},
       {name: "Gross Turnover", value: 11, turnover: 1},
       {name: "Legals?", value: 12, section: 1}
    ]

  end
end

@config_file = WrbIlab::Setup.new
